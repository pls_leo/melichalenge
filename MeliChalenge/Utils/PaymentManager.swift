//
//  PaymentManager.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit

class PaymentManager: NSObject {
    
    public static let sharedInstance = PaymentManager()
    public var amount: Float = 0
    public var card: Card?
    public var bank: Bank?
    public var installment: Installment?
    
    //This prevents others from using the default '()' initializer for this class.
    private override init() {}
    
    func clean() {
        amount = 0
        card = nil
        bank = nil
        installment = nil
    }
}

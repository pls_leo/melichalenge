//
//  Card.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit

struct Card {
    
    let id: String
    let type: String
    let name: String
    let icon: String
    let length: Int
    let pattern: String
    let cvvLength: Int
    let cvvLocation: String
    
    init(dictionary: [String: Any]) {
        id = dictionary["id"] as! String
        type = dictionary["payment_type_id"] as! String
        name = dictionary["name"] as! String
        icon = dictionary["secure_thumbnail"] as! String
        let settings = dictionary["settings"] as! [[String: Any]]
        let cardSettings = settings[0]["card_number"] as! [String: Any]
        length = cardSettings["length"] as! Int
        let binSettings = settings[0]["bin"] as! [String: Any]
        pattern = binSettings["pattern"] as! String
        let cvvSettings = settings[0]["security_code"] as! [String: Any]
        cvvLength = cvvSettings["length"] as! Int
        cvvLocation = cvvSettings["card_location"] as! String
    }

}

//
//  Installment.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import Foundation

struct Installment {
    
    let installments: Int
    let message: String
    
    init(dictionary: [String: Any]) {
        installments = dictionary["installments"] as! Int
        message = dictionary["recommended_message"] as! String
    }
    
}

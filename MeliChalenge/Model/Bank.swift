//
//  Bank.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit

struct Bank {
    
    let id: String
    let name: String
    let icon: String
    
    init(dictionary: [String: Any]) {
        id = dictionary["id"] as! String
        name = dictionary["name"] as! String
        icon = dictionary["secure_thumbnail"] as! String
    }

}

//
//  CardService.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit

class CardService: BaseService {

    func getCreditCards(callback:@escaping (_ success: Bool, _ data: Array<Card>?, _ errorMsg: String?) -> Void) {
        let params: [String : Any] = ["public_key": PUBLICK_KEY]
        getFromService(baseURL: DOMAIN, uri: "v1/payment_methods", parameters: params) { success, data, errorMsg in
            if (success) {
                var creditCards: Array<Card> = []
                if let dataArr = data as? [[String: Any]] {
                    for card in dataArr {
                        if let type = card["payment_type_id"] as? String {
                            if (type == "credit_card" ) {
                                creditCards.append(Card(dictionary: card))
                            }
                        }
                    }
                }
                callback(true, creditCards, nil)
            } else {
                callback(false, nil, errorMsg)
            }
        }
    }
}

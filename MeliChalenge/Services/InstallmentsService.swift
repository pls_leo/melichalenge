//
//  InstallmentsService.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit

class InstallmentsService: BaseService {
    
    func getInstallments(card: Card, bank: Bank, amount: Float, callback:@escaping (_ success: Bool, _ data: Array<Installment>?, _ errorMsg: String?) -> Void) {
        let params: [String : Any] = ["public_key": PUBLICK_KEY,
                                      "payment_method_id": card.id,
                                      "amount": amount,
                                      "issuer.id": bank.id]
        getFromService(baseURL: DOMAIN, uri: "v1/payment_methods/installments", parameters: params) { success, data, errorMsg in
            if (success) {
                var installments: Array<Installment> = []
                if let dataArr = data as? [[String: Any]] {
                    if let insArr = dataArr[0]["payer_costs"] as? [[String: Any]] {
                        for installment in insArr {
                            installments.append(Installment(dictionary: installment))
                        }
                    }
                }
                callback(true, installments, nil)
            } else {
                callback(false, nil, errorMsg)
            }
        }
    }
}

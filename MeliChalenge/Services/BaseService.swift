//
//  BaseService.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 22/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit
import Alamofire

class BaseService: NSObject {

    let PUBLICK_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88"
    let DOMAIN = "api.mercadopago.com"
    
    func getFromService(baseURL: String, uri: String, parameters: [String : Any], callback:@escaping (_ success: Bool, _ data: Any?, _ errorMsg: String?) -> Void) {
        
        let params = parameters.map{ "\($0)=\($1)" }.joined(separator: "&")
        let serviceURL = "https://\(baseURL)/\(uri)?\(params)"
        
        Alamofire.request(serviceURL, method: .get).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                if let json = response.result.value {
                    if let dictionary = json as? [[String: Any]] {
                         callback(true, dictionary, nil)
                    } else {
                        callback(false, nil, "Service error")
                    }
                }
            case .failure(let error):
                callback(false, nil, error.localizedDescription)
            }
        }
    }
}

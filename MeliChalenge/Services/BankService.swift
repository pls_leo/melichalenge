//
//  BankService.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit

class BankService: BaseService {
   
    func getBanks(card: Card, callback:@escaping (_ success: Bool, _ data: Array<Bank>?, _ errorMsg: String?) -> Void) {
        let params: [String : Any] = ["public_key": PUBLICK_KEY,
                                      "payment_method_id": card.id]
        getFromService(baseURL: DOMAIN, uri: "v1/payment_methods/card_issuers", parameters: params) { success, data, errorMsg in
            if (success) {
                var banks: Array<Bank> = []
                if let dataArr = data as? [[String: Any]] {
                    for bank in dataArr {
                            banks.append(Bank(dictionary: bank))
                    }
                }
                callback(true, banks, nil)
            } else {
                callback(false, nil, errorMsg)
            }
        }
    }
}

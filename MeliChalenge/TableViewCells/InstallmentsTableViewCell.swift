//
//  InstallmentsTableViewCell.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit

class InstallmentsTableViewCell: UITableViewCell {

    @IBOutlet weak var installmentLbl: UILabel!
    
    private var installment: Installment?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if (selected) {
            PaymentManager.sharedInstance.installment = installment
        }
    }

    func updateInstallment(aInstallment: Installment) {
        self.installment = aInstallment
        installmentLbl.text = aInstallment.message
    }
}

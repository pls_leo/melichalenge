//
//  BankTableViewCell.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit

class BankTableViewCell: UITableViewCell {

    @IBOutlet weak var bankImg: UIImageView!
    @IBOutlet weak var bankLbl: UILabel!
    
    private var bank: Bank?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if (selected) {
            PaymentManager.sharedInstance.bank = bank
        }
    }
    
    func updateBank(aBank: Bank) {
        self.bank = aBank
        bankLbl.text = aBank.name
        if let bankPic = URL(string: aBank.icon) {
            bankImg.af_setImage(
                withURL: bankPic,
                placeholderImage: UIImage(named:"defaultBank"),
                filter: nil,
                imageTransition: .crossDissolve(0.5)
            )
        } else {
            bankImg.image = UIImage(named:"defaultBank")
        }
    }

}

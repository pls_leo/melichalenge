//
//  CardTableViewCell.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit
import AlamofireImage

class CardTableViewCell: UITableViewCell {

    @IBOutlet weak var cardImg: UIImageView!
    @IBOutlet weak var brandLbl: UILabel!
    
    private var card: Card?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if (selected) {
            PaymentManager.sharedInstance.card = card
        }
    }
    
    func updateCard(aCard: Card) {
        self.card = aCard
        brandLbl.text = aCard.name
        if let cardPic = URL(string: aCard.icon) {
            cardImg.af_setImage(
                withURL: cardPic,
                placeholderImage: UIImage(named:"defaultCard"),
                filter: nil,
                imageTransition: .crossDissolve(0.5)
            )
        } else {
            cardImg.image = UIImage(named:"defaultCard")
        }
    }
}

//
//  CardDataViewController.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 25/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit
import PKHUD

class CardDataViewController: UIViewController {

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var cardNumberTf: UITextField!
    @IBOutlet weak var monthTf: UITextField!
    @IBOutlet weak var yearTf: UITextField!
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var cvvTf: UITextField!
    @IBOutlet weak var documentTf: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Datos de Tarjeta"
        
        // set gesture recognizer for keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.screenTapped))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
    }
    
    @objc func screenTapped() {
        self.view.endEditing(true)
    }
    
    @IBAction func cvvTooltip(_ sender: Any) {
        let position = PaymentManager.sharedInstance.card?.cvvLocation == "back" ? "dorso" : "frente"
        HUD.flash(.label("\(PaymentManager.sharedInstance.card!.cvvLength) dígitos ubicados al \(position) de la tarjeta"), delay: 2)
    }
    
    @IBAction func onClickContinue(_ sender: Any) {
        let pattern = PaymentManager.sharedInstance.card!.pattern
        let range = self.cardNumberTf.text!.range(of: pattern, options: .regularExpression)
        if (range == nil) {
            HUD.flash(.label("La tarjeta ingresada es inválida"), delay: 2)
            return;
        }
        let dateComponents = Calendar.current.dateComponents([.year, .month], from: Date())
        let year = (Int(yearTf.text!) ?? 0) + 2000
        let month = Int(monthTf.text!) ?? 0
        if (year < dateComponents.year! || (year == dateComponents.year! && month < dateComponents.month!)) {
            HUD.flash(.label("La fecha de vencimiento ingresada es inválida"), delay: 2)
            return;
        }
        if (nameTf.text!.isEmpty) {
            HUD.flash(.label("Por favor ingrese un nombre"), delay: 2)
            return;
        }
        if (cvvTf.text!.isEmpty || cvvTf.text!.count < PaymentManager.sharedInstance.card!.cvvLength) {
            HUD.flash(.label("El código de seguridad ingresado es inválido"), delay: 2)
            return;
        }
        if (documentTf.text!.isEmpty) {
            HUD.flash(.label("Por favor ingrese un número de documento"), delay: 2)
            return;
        }
        let message = "Se realizó el pago de $\(PaymentManager.sharedInstance.amount) con \(PaymentManager.sharedInstance.card!.name) de \(PaymentManager.sharedInstance.bank!.name) en \(PaymentManager.sharedInstance.installment!.message)"
        HUD.flash(.label(message), delay: 5)
        
        PaymentManager.sharedInstance.clean()
        navigationController?.popToRootViewController(animated: true)
    }
}

extension CardDataViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string.isEmpty) {
            return true
        }
        
        let currentText = textField.text ?? ""
        let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        switch textField {
        case self.cardNumberTf:
            return replacementText.count <= PaymentManager.sharedInstance.card!.length
        case self.monthTf:
            let month = Int(replacementText) ?? 0
            return month <= 12
        case self.yearTf:
            return replacementText.count <= 2
        case self.cvvTf:
            return replacementText.count <= PaymentManager.sharedInstance.card!.cvvLength
        default:
            return true
        }
    }
}

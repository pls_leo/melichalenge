//
//  ViewController.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 22/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.openHome), userInfo: nil, repeats: false)

    }

    @objc func openHome() {
        performSegue(withIdentifier: "openAmount", sender: self)
    }

}


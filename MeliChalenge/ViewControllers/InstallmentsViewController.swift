//
//  InstallmentsViewController.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit
import PKHUD

class InstallmentsViewController: UIViewController {

    @IBOutlet weak var cardImg: UIImageView!
    @IBOutlet weak var bankImg: UIImageView!
    @IBOutlet weak var bankLbl: UILabel!
    @IBOutlet weak var installmentsTbl: UITableView!
    
    private var installments: Array<Installment> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Cuotas"

        if let cardPic = URL(string: PaymentManager.sharedInstance.card!.icon) {
            cardImg.af_setImage(
                withURL: cardPic,
                placeholderImage: UIImage(named:"defaultCard"),
                filter: nil,
                imageTransition: .crossDissolve(0.5)
            )
        } else {
            cardImg.image = UIImage(named:"defaultCard")
        }
        if let bankPic = URL(string: PaymentManager.sharedInstance.bank!.icon) {
            bankImg.af_setImage(
                withURL: bankPic,
                placeholderImage: UIImage(named:"defaultBank"),
                filter: nil,
                imageTransition: .crossDissolve(0.5)
            )
        } else {
            bankImg.image = UIImage(named:"defaultBank")
        }
        bankLbl.text = "\(PaymentManager.sharedInstance.card!.name) de \(PaymentManager.sharedInstance.bank!.name)"
        
        fetchInstallments()
    }
    
    func fetchInstallments() {
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        let service = InstallmentsService()
        service.getInstallments(card: PaymentManager.sharedInstance.card!, bank: PaymentManager.sharedInstance.bank!, amount: PaymentManager.sharedInstance.amount) { success, data, errorMsg in
            PKHUD.sharedHUD.hide()
            if (success) {
                self.installments = data ?? []
                self.installmentsTbl.reloadData()
            } else {
                HUD.flash(.labeledError(title: errorMsg, subtitle: nil), delay: 2)
            }
        }
    }

    @IBAction func onClickContinue(_ sender: Any) {
        if (PaymentManager.sharedInstance.installment == nil) {
            HUD.flash(.label("Por favor seleccione las cuotas"), delay: 2)
        } else {
            performSegue(withIdentifier: "openCardData", sender: self)
        }
    }
}

// MARK: UITableViewDataSource Methods
extension InstallmentsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return installments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let aInstallment = installments[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "InstallmentsTableViewCell",
                                                 for: indexPath) as! InstallmentsTableViewCell
        cell.updateInstallment(aInstallment: aInstallment)
        return cell
        
    }
}

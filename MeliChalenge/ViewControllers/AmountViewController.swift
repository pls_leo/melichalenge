//
//  AmountViewController.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 22/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit
import PKHUD

class AmountViewController: UIViewController {

    @IBOutlet weak var amountTf: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.viewControllers = [self];

        self.navigationItem.title = "Monto"
        
        self.amountTf.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        // set gesture recognizer for keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.screenTapped))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        amountTf?.text = String(PaymentManager.sharedInstance.amount).currencyInputFormatting()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let amountString = self.amountTf.text?.currencyInputFormatting() {
            self.amountTf.text = amountString
        }
    }
    
    @objc func screenTapped() {
        self.view.endEditing(true)
    }

    @IBAction func onClickContinue(_ sender: Any) {
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = "$"
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        number = formatter.number(from: (amountTf?.text)!)
        
        if (number == 0) {
            HUD.flash(.label("Por favor ingrese un monto"), delay: 2)
        } else {
            PaymentManager.sharedInstance.amount = number.floatValue
            performSegue(withIdentifier: "openCards", sender: self)
        }
    }
}

//
//  BankViewController.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit
import PKHUD

class BankViewController: UIViewController {

    @IBOutlet weak var cardImg: UIImageView!
    @IBOutlet weak var cardBrandLbl: UILabel!
    @IBOutlet weak var banksTbl: UITableView!
    @IBOutlet weak var continueBtn: UIButton!
    
    private var banks: Array<Bank> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Banco"
        
        if let cardPic = URL(string: PaymentManager.sharedInstance.card!.icon) {
            cardImg.af_setImage(
                withURL: cardPic,
                placeholderImage: UIImage(named:"defaultCard"),
                filter: nil,
                imageTransition: .crossDissolve(0.5)
            )
        } else {
            cardImg.image = UIImage(named:"defaultCard")
        }
        cardBrandLbl.text = PaymentManager.sharedInstance.card!.name
        
        fetchBanks()
    }
    
    func fetchBanks() {
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        let service = BankService()
        service.getBanks(card: PaymentManager.sharedInstance.card!) { success, data, errorMsg in
            PKHUD.sharedHUD.hide()
            if (success) {
                self.banks = data ?? []
                if (self.banks.count == 0) {
                    self.continueBtn.isEnabled = false
                    HUD.flash(.label("No se encontraron bancos para la tarjeta seleccionada"), delay: 3)
                } else {
                    self.banksTbl.reloadData()
                }
            } else {
                HUD.flash(.labeledError(title: errorMsg, subtitle: nil), delay: 3)
            }
        }
    }
    
    @IBAction func onClickContinue(_ sender: Any) {
        
        if (PaymentManager.sharedInstance.bank == nil) {
            HUD.flash(.label("Por favor seleccione un banco"), delay: 2)
        } else {
            performSegue(withIdentifier: "openInstallments", sender: self)
        }
    }
}

// MARK: UITableViewDataSource Methods
extension BankViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return banks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let aBank = banks[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "BankTableViewCell",
                                                 for: indexPath) as! BankTableViewCell
        cell.updateBank(aBank: aBank)
        return cell
        
    }
}

//
//  CardsViewController.swift
//  MeliChalenge
//
//  Created by Leando Pardo on 24/03/2019.
//  Copyright © 2019 Leando Pardo. All rights reserved.
//

import UIKit
import PKHUD

class CardsViewController: UIViewController {

    @IBOutlet weak var cardsTbl: UITableView!
    
    private var cards: Array<Card> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Medio de Pago"

        fetchCards()
    }
    
    func fetchCards() {
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        let service = CardService()
        service.getCreditCards() { success, data, errorMsg in
            PKHUD.sharedHUD.hide()
            if (success) {
                self.cards = data ?? []
                self.cardsTbl.reloadData()
            } else {
                HUD.flash(.labeledError(title: errorMsg, subtitle: nil), delay: 2)
            }
        }
    }
    
    @IBAction func onClickContinue(_ sender: Any) {
        
        if (PaymentManager.sharedInstance.card == nil) {
            HUD.flash(.label("Por favor seleccione una tarjeta"), delay: 2)
        } else {
            performSegue(withIdentifier: "openBank", sender: self)
        }
    }
}

// MARK: UITableViewDataSource Methods
extension CardsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let aCard = cards[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardTableViewCell",
                                                     for: indexPath) as! CardTableViewCell
            cell.updateCard(aCard: aCard)
            return cell
        
    }
}
